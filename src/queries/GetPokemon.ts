import { gql } from "apollo-boost";

export interface Pokemon {
  number: string;
  name: string;
  image: string;
  classification: string;
  types: string[];
}

export interface GetPokemonData {
  pokemon: Pokemon;
}

export interface GetPokemonVars {
  id: string;
}

const GET_POKEMON = gql`
  query getPokemon($id: String!) {
    pokemon(id: $id) {
      number
      name
      image
      classification
      types
    }
  }
`;

export const mocks = [
  {
    request: {
      query: GET_POKEMON,
      variables: { id: "1" },
    },
    result: {
      data: {
        pokemon: {
          number: "001",
          name: "Bulbasaur",
          types: ["Grass", "Poison"],
          classification: "Seed Pokémon",
          image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        },
      },
    },
  },
  {
    request: {
      query: GET_POKEMON,
      variables: { id: "2" },
    },
    error: new Error("Something went wrong"),
  },
];

export default GET_POKEMON;
