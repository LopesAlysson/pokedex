import React, { useState } from "react";
import styled, { css, keyframes } from "styled-components";
import { useQuery } from "@apollo/react-hooks";

import { InfoRow, Value, Label } from "./Commons";
import { ReactComponent as Pokeball } from "../assets/pokeball.svg";
import GetPokemon, { GetPokemonData, GetPokemonVars } from "../queries/GetPokemon";

interface CardProps {
  onClick: (id: string) => void;
  id: string;
}

const CardContainer = styled.div<{ isLoading: boolean; error: boolean }>`
  min-width: 400px;
  height: 500px;
  margin: 4px;
  flex: 1;

  border-radius: 8px;
  background-color: #fff;
  border: 1px solid #eee;
  box-shadow: 0 0 8px rgba(0, 0, 0, 0.1);

  transform: scale(0.9);
  transition: all ease-in-out 300ms;
  ${(props) =>
    !props.isLoading &&
    !props.error &&
    css`
      &:hover {
        box-shadow: 0 0 16px rgba(0, 0, 0, 0.3);
        transform: scale(1);
        cursor: pointer;
        & > img {
          opacity: 1;
          transform: scale(1);
          filter: grayscale(0%);
        }
      }
    `}
`;

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const LoadingContainer = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  flex: 1;
`;

const StyledPokeball = styled(Pokeball)`
  animation: ${rotate} 750ms linear infinite;
`;

const ErrorContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  align-items: center;
  justify-content: center;
  flex: 1;
`;

const ErrorText = styled.span`
  font-family: Lato;
  font-weight: bold;
  color: #597183;
  font-size: 2rem;
  margin-bottom: 16px;
`;

const RetryButton = styled.button`
  margin-top: 16px;
  padding: 12px 16px;
  background-color: #159ce4;
  border-radius: 3px;
  border: none;
  color: #fff;
  font-family: Lato;
  font-size: 1.2rem;
  transition: all ease-in-out 300ms;

  &:hover {
    cursor: pointer;
    background-color: #3db5eb;
  }

  &:focus {
    background-color: #147ec1;
  }
`;

const CardHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const Pokemon = {
  Name: styled.h2`
    font-family: Poppins;
    font-weight: 300;
    color: #597183;
    font-size: 2.1rem;
    width: 60%;
    padding: 8px;
    margin: 0 auto;
  `,

  Number: styled.h3`
    font-family: Lato;
    font-weight: bold;
    font-size: 4rem;
    margin: 0 auto;
    color: #597183;
    width: 40%;
    padding: 8px;
    align-self: flex-start;
  `,

  Image: styled.img<{ imageLoaded: boolean }>`
    opacity: ${(props) => (props.imageLoaded ? 0.85 : 0)};
    z-index: 2;
    filter: grayscale(25%);
    transform: scale(0.8);
    transition: all ease-out 300ms;
    margin: 16px auto;
    max-width: 344px;
    max-height: 320px;
    height: auto;
    width: auto;
  `,

  ImagePlaceholder: styled(StyledPokeball).attrs({
    width: 260,
  })<{ imageLoaded: boolean }>`
    position: absolute;
    bottom: 16px;
    left: 70px;
    opacity: ${(props) => (props.imageLoaded ? 0 : 0.5)};
    z-index: 1;
  `,
};

const Card: React.FC<CardProps> = ({ onClick, id }) => {
  const [imageLoaded, setImageLoaded] = useState(false);
  const { loading, error, data, refetch, networkStatus } = useQuery<GetPokemonData, GetPokemonVars>(GetPokemon, {
    variables: { id },
    notifyOnNetworkStatusChange: true,
  });
  const hasError = Boolean(error);

  function handleCardClick() {
    onClick(id);
  }

  function handleImageLoaded() {
    setImageLoaded(true);
  }

  function handleRefetch() {
    refetch();
  }

  if (loading || networkStatus === 4) {
    return (
      <CardContainer isLoading={loading} error={hasError} onClick={handleCardClick}>
        <LoadingContainer data-testid="loading-container">
          <StyledPokeball data-testid="styled-pokeball" width={180} />
        </LoadingContainer>
      </CardContainer>
    );
  }

  if (hasError || !data) {
    return (
      <CardContainer isLoading={loading} error={hasError} onClick={handleCardClick}>
        <ErrorContainer>
          <ErrorText data-testid="error-text">
            Oops...
            <br /> Algo deu errado.
          </ErrorText>
          <RetryButton data-testid="retry-button" onClick={handleRefetch}>
            tentar novamente
          </RetryButton>
        </ErrorContainer>
      </CardContainer>
    );
  }

  return (
    <CardContainer isLoading={loading} error={hasError} onClick={handleCardClick}>
      <CardHeader>
        <Pokemon.Name data-testid="pokemon-name">{data.pokemon.name}</Pokemon.Name>
        <Pokemon.Number data-testid="pokemon-number">#{data.pokemon.number}</Pokemon.Number>
      </CardHeader>
      {data.pokemon.types.length === 1 ? (
        <InfoRow>
          <Label>Type:</Label>
          <Value data-testid="pokemon-types">{data.pokemon.types[0]}</Value>
        </InfoRow>
      ) : (
        <InfoRow>
          <Label>Types:</Label>
          <Value data-testid="pokemon-types">{data.pokemon.types.join(", ")}</Value>
        </InfoRow>
      )}
      <InfoRow>
        <Label>Classification:</Label>
        <Value data-testid="pokemon-classification">{data.pokemon.classification}</Value>
      </InfoRow>
      <Pokemon.ImagePlaceholder imageLoaded={imageLoaded} />
      <Pokemon.Image
        data-testid="pokemon-image"
        onLoad={handleImageLoaded}
        imageLoaded={imageLoaded}
        src={data.pokemon.image}
      />
    </CardContainer>
  );
};

export default Card;
