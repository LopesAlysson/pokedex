import styled from "styled-components";

export const Label = styled.span`
  font-family: Lato;
  font-size: 1.2rem;
  font-weight: bold;
  color: #597183;
`;

export const Value = styled.span`
  margin-left: 8px;
  font-family: Lato;
  font-size: 1.2rem;
  font-weight: 400;
  color: #32383c;
`;

export const InfoRow = styled.div`
  display: flex;
  align-items: center;
  padding: 5px 16px;
`;
