import React from "react";
import { MockedProvider } from "@apollo/react-testing";
import { act, render } from "@testing-library/react";
import { mocks } from "../queries/GetPokemon";
import Card from "./Card";

async function wait(ms = 0) {
  await act(() => {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  });
}

const onClick = jest.fn();

it("renders correctly", async () => {
  const { container, getByTestId } = render(
    <MockedProvider addTypename={false} mocks={mocks}>
      <Card id="1" onClick={onClick} />
    </MockedProvider>
  );

  expect(getByTestId("loading-container")).toBeTruthy();

  await wait();

  expect(getByTestId("pokemon-name").textContent).toBe("Bulbasaur");
  expect(getByTestId("pokemon-number").textContent).toBe("#001");
  expect(getByTestId("pokemon-types").textContent).toBe("Grass, Poison");
  expect(getByTestId("pokemon-classification").textContent).toBe("Seed Pokémon");
  expect(getByTestId("pokemon-image")).toHaveAttribute("src", "https://img.pokemondb.net/artwork/bulbasaur.jpg");

  expect(container.firstChild).toMatchSnapshot();
});

it("renders error", async () => {
  const { container, getByTestId } = render(
    <MockedProvider addTypename={false} mocks={mocks}>
      <Card id="2" onClick={onClick} />
    </MockedProvider>
  );

  expect(getByTestId("loading-container")).toBeTruthy();

  await wait();

  expect(getByTestId("error-text")).toBeTruthy();
  expect(getByTestId("retry-button")).toBeTruthy;
  expect(container.firstChild).toMatchSnapshot();
});
