import React from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "@apollo/react-hooks";

import "./index.css";
import App from "./App";
import { GraphQLClient } from "./constants";

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={GraphQLClient}>
      <App />
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
