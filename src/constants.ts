import ApolloClient from "apollo-boost";

export const API_URL = process.env.REACT_APP_API_URL;
export const GraphQLClient = new ApolloClient({
  uri: API_URL,
});
